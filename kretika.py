#! /usr/bin/env python3

import asyncio
import os.path

import aiohttp

from bs4 import BeautifulSoup


url = 'http://64.244.59.70/IMH/article.aspx'

async def fetch_page(session, url, **kwargs):
    with aiohttp.Timeout(10):
        async with session.get(url, **kwargs) as response:
            assert response.status == 200
            return await response.text()

async def fetch_pdf(session, url, filename):
    chunk_size = 65536
    with aiohttp.Timeout(1200):
        async with session.get(url) as response:
            assert response.status == 200
            with open(filename, 'wb') as fd:
                while True:
                    chunk = await response.content.read(chunk_size)
                    if not chunk:
                        break
                    fd.write(chunk)
                print(filename)

loop = asyncio.get_event_loop()
conn = aiohttp.TCPConnector(limit=60)
with aiohttp.ClientSession(loop=loop, connector=aiohttp.TCPConnector()) as session:
    for n in range(1001, 1406):
        params = {'artid': n,
          'l': 'En'}
        content = loop.run_until_complete(
            fetch_page(session, url, params=params))
        soup = BeautifulSoup(content, 'html5lib')
        rows = soup.find_all('tr', attrs={'bgcolor': '#e6e5c6'})
        data = {}
        data['title'] = rows[1].text[:50]
        data['abstract'] = rows[2].text
        data['author'], data['section'], data['volume'], data['pages'], data['year'], data['lang'] = list(map(lambda x: x.text, rows[0].find_all('td')))
        pdf_a = soup.find('a', attrs={'class': 'file'})
        pdf_url = pdf_a.attrs['onclick'][13:37]
        filename = '{volume} {author} ({year}) {title}.pdf'.format(**data)
        pdf_url = 'http://64.244.59.70/IMH/{}'.format(pdf_url)
        if not os.path.exists(filename):
            loop.run_until_complete(fetch_pdf(session, pdf_url, filename))
