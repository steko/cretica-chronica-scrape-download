A Python scraper and downloader for Κρητικά Χρονικά/Cretica Chronica, a journal about the history and archaeology of Crete.

The scraper takes advantage of asyncio and downloads documents from http://online.cretica-chronica.gr/
